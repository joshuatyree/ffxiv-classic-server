require("global");
require("weaponskill");

function onSkillPrepare(caster, target, skill)
    return 0;
end;

function onSkillStart(caster, target, skill)
    return 0;
end;

function onPositional(caster, target, skill)
    skill.basePotency = skill.basePotency * 1.25;
end;

function onSkillFinish(caster, target, skill, action, actionContainer)
    action.amount = CalculateDamage(caster, target, skill, action)

    --DoAction handles rates, buffs, dealing damage
    action.DoAction(caster, target, skill, actionContainer);
end;
