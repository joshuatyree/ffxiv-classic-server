﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFXIVClassic_Map_Server.actors.chara.ai.utils
{
    static class StatUtils
    {
        public static Dictionary<Job, Modifier> PrimaryAttackMods = new Dictionary<Job, Modifier>()
        {
            { Job.Pugilist, Modifier.Intelligence },
            { Job.Monk, Modifier.Intelligence },
            { Job.Gladiator, Modifier.Mind },
            { Job.Paladin, Modifier.Mind },
            { Job.Marauder, Modifier.Vitality },
            { Job.Warrior, Modifier.Vitality },
            { Job.Archer, Modifier.Dexterity },
            { Job.Bard, Modifier.Dexterity },
            { Job.Lancer, Modifier.Piety },
            { Job.Dragoon, Modifier.Piety },
            { Job.Conjurer, Modifier.Mind },
            { Job.WhiteMage, Modifier.Mind },
            { Job.Thaumaturge, Modifier.Mind },
            { Job.BlackMage, Modifier.Mind }
        };

        public static Dictionary<Job, Modifier> SecondaryAttackMods = new Dictionary<Job, Modifier>()
        {
            { Job.Pugilist, Modifier.Strength },
            { Job.Monk, Modifier.Strength },
            { Job.Gladiator, Modifier.Strength },
            { Job.Paladin, Modifier.Strength },
            { Job.Marauder, Modifier.Strength },
            { Job.Warrior, Modifier.Strength },
            { Job.Archer, Modifier.Piety },
            { Job.Bard, Modifier.Piety },
            { Job.Lancer, Modifier.Strength },
            { Job.Dragoon, Modifier.Strength },
            { Job.Conjurer, Modifier.Piety },
            { Job.WhiteMage, Modifier.Piety },
            { Job.Thaumaturge, Modifier.Piety },
            { Job.BlackMage, Modifier.Piety }
        };
    }
}
