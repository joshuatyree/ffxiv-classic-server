﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFXIVClassic_Map_Server.actors.chara;

namespace FFXIVClassic_Map_Server.dataobjects
{
    class BaseResistance
    {
        public readonly uint id;
        public readonly uint guardianID;
        public readonly uint attributeID;
        public readonly uint value;
        public Modifier Modifier => (Modifier)attributeID;
        public Guardian Guardian => (Guardian)guardianID;

        public BaseResistance(MySqlDataReader reader)
        {
            id = reader.GetUInt32("id");
            guardianID = reader.GetUInt32("guardianID");
            attributeID = reader.GetUInt32("attributeID");
            value = reader.GetUInt32("value");
        }
    }
}
