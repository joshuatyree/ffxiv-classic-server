﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using FFXIVClassic_Map_Server.actors.chara;

namespace FFXIVClassic_Map_Server.dataobjects
{
    class TribeDStat
    {
        public readonly uint id;
        public readonly uint attributeID;
        public readonly uint tribeID;
        public readonly uint dStat;
        public Modifier Modifier => (Modifier)attributeID;
        public Tribe Tribe => (Tribe)tribeID;

        public TribeDStat(MySqlDataReader reader)
        {
            id = reader.GetUInt32("id");
            attributeID = reader.GetUInt32("attributeID");
            tribeID = reader.GetUInt32("tribeID");
            dStat = reader.GetUInt32("dstat");
        }
    }
}
